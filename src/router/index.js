import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const user = window.localStorage.getItem('user');
const user_id = JSON.parse(user);

const ifAuthenticated = (to, from, next) => {
  if (user_id) {
    next()
    return
  }
  next('/')
}

const routes = [
  {
    path: '/',
    name: 'Main',
    component: () => import('@/views/Main.vue')
  },
  {
    path: '/catalog/:id',
    name: 'Catalog',
    component: () => import('@/views/Catalog.vue')
  },
  {
    path: '/solution/:id',
    name: 'Solution',
    component: () => import('@/views/PageSolution.vue')
  },
  {
    path: '/private-office/:type',
    name: 'Private-office',
    component: () => import('@/views/PrivateOffice.vue'),
    beforeEnter: ifAuthenticated,
  },
  {
    path: '/innovator',
    name: 'Innovator',
    component: () => import('@/views/Innovator.vue')
  },
  {
    path: '/customer',
    name: 'Customer',
    component: () => import('@/views/Customer.vue')
  },
  {
    path: '/investors',
    name: 'Investors',
    component: () => import('@/views/Investors.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior() {
    return { x: 0, y: 0 }
  }
})

export default router
