import axios from '@/service/axios';

class Req {
  async get(url) {
    return await axios.get(`/${url}/`);
  }

  async getParams(url, params) {
    return await axios.get(`/${url}/`, {
      params: { ...params }
    });
  }

  async post(url, data) {
    return await axios.post(`/${url}/`, data);
  }

  async delete(url, params) {
    return await axios.delete(`/${url}/`, {
      params: params
    });
  }

  async patch(url, data) {
    return await axios.patch(`/${url}/`, data);
  }
}

class User extends Req {
  registery(data) {
    return this.post('profile', { ...data });
  }

  login(data) {
    return this.post('login', { ...data });
  }

  getUserData(id) {
    return this.get(`login/${ id }`);
  }

  logout() {
    return this.post('logout');
  }

  changePassword(id, data) {
    return this.patch(`login/${ id }`, data)
  }
}

class Category extends Req {
  async getAllCategory() {
    return this.get('category');
  }

  addCategory(category) {
    return this.post('category', { category });
  }

  getSelectCategory(id) {
    return this.get(`category/${ id }`);
  }
}

class Project extends Req {
  getAllProjects() {
    return this.get('project');
  }

  getSelectProject(id) {
    return this.get(`project/${ id }`);
  }

  getProgectInCategory(id_category) {
    return this.getParams('project/', { category: id_category });
  }
   
  addProject(data) {
    return this.post('project', { ...data });
  }

  removeProject(data) {
    return this.delete(`project/${data}`);
  }
}

class Tag extends Req {
  getAllTag() {
    return this.get('tag')
  }
  
  addTag(data) {
    return this.post('tag', { ...data })
  }
}

class Favorite extends Req {
  addFavoriteProject(data) {
    return this.post('favorite_project', { ...data });
  }

  getFavoriteProject(data) {
    return this.getParams('favorite_project', { user: data });
  }

  removeFavoriteProject(data) {
    return this.delete('favorite_project', { ...data });
  }
}

class UrData extends Req {
  getUrData(id) {
    return this.get(`ur_data/${ id }`);
  }

  addUrData(data) {
    return this.post('ur_data', { ...data });
  } 
}

const user = new User();
const category = new Category();
const project = new Project();
const tag = new Tag();
const favorite = new Favorite();
const urData = new UrData();

export {
  user,
  category,
  project,
  tag,
  favorite,
  urData,
}


